import React, { Component } from "react";
import UserItem from "../UserItem/UserItem";
import axios from "axios";
import PieChart from 'react-minimal-pie-chart';

export class UserList extends Component {
  constructor(props) {
    super(props);
    this.state = { users: [], fetching: true };
    this.getRandomUsers = this.getRandomUsers.bind(this);
  }

  componentDidMount() {
    //request random users
    //set state to fetching
    this.getRandomUsers();
  }

  getRandomUsers() {
    axios
      .get(`https://randomuser.me/api/?results=100`)
      .then(response => {
        console.log(response);
        this.setState({
          users: response.data.results,
          fetching: false
        });

        console.log(this.state.users);
      })
      .catch(error => {
        console.log(error);
      });
  }

  renderChart() {
    var females = this.state.users.filter(user => {
      return (user.gender = "female");
    });
    var males = this.state.users.filter(user => {
      return (user.gender = "male");
    });

    const fe = females.length;
    const ma = males.length;

    console.log(fe);

    return (
      <div>
        <PieChart
          data={[
            { title: "Male", value: fe, color: "#E38627" },
            { title: "Female", value: ma, color: "#C13C37" },
          ]}
        />
        ;
      </div>
    );
  }

  renderUserItems() {
    return this.state.users.map((user, index) => {
      const first = user.name.first;
      const last = user.name.last;
      return (
        <UserItem
          key={first + last + index}
          title={user.name.title}
          firstName={first}
          lastName={last}
          thumbnail={user.picture.thumbnail}
        />
      );
    });
  }

  render() {
    if (this.state.fetching) {
      //show loader
      return (
        <div>
          <img src="./Loading.gif" />
        </div>
      );
    }

    return (
      <div className="header">
        <h2>Users</h2>
        <ul>{this.renderUserItems()}</ul>

        <div style={{float: 'right'}}>
        <h2>Chart</h2>
        {this.renderChart()}
        <div />
        </div>
      </div>
    );
  }
}
