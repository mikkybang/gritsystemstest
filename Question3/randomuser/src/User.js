import axios from 'axios'

export function getRandomUsers() {
   axios.get(`https://randomuser.me/api/?results=100`)
    .then(response => {
      return response.json();
    })
    .catch(error => {
      console.log(error);
    });
}
