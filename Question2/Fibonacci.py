
'''This Function below takes a number n and '''
def Fibonacci(n):
    if n == 0:
        return 0
    if n == 1:
        return 1
    print(n, sep=",", end=" " )
    return  Fibonacci(n-1) + Fibonacci(n-2)
    

def main():
    n = int(input("Enter the number which you want to find the Fibonacci Sequence For: "))
    print(Fibonacci(n))



if __name__ == "__main__":
    main()